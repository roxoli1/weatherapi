package com.example.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableCaching
public class WeatherApiDemo1Application {

	public static void main(String[] args) {
		SpringApplication.run(WeatherApiDemo1Application.class, args);
	}
}
