package com.example.application.controller;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {
	
	public Weather() {
	}

	public Weather(CurrentObservation currentObservation) {
		this.currentObservation = currentObservation;
	}
	

	private Response response;
	
	@JsonProperty(value = "current_observation")
	private  CurrentObservation currentObservation;

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public CurrentObservation getCurrentObservation() {
		return currentObservation;
	}

	public void setCurrentObservation(CurrentObservation currentObservation) {
		this.currentObservation = currentObservation;
	}



}
