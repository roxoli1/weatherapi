package com.example.application.controller;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentObservation {

	public CurrentObservation() {
	}
	
	public CurrentObservation(String weather) {
		this.weather = weather;
	}

	private String weather;

	public String getWeather() {
		return weather;
	}

	public void setWeather(String weather) {
		this.weather = weather;
	}
}
