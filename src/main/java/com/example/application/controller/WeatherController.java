package com.example.application.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class WeatherController {
	// @RequestMapping(value="/{city}", method = RequestMethod.GET)
	// public Weather printWeather(@PathVariable String city) {
	// return weatherService.printWeather(city);
	// }
	private String[] state = { "AL", "AK", "AZ", "AR", "CA", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN",
			"IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY",
			"NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY",
			"AS", "DC", "FM", "GU", "MH", "MP", "PW", "PR", "VI" };

	private Weather observer = null;

	@RequestMapping(value = "/{city}", method = RequestMethod.GET)
	// @Cacheable("weather")
	public Weather printWeather(@PathVariable String city) {
		RestTemplate current = new RestTemplate();

		for (int i = 0; i < state.length - 1; i++) {

			String url = "http://api.wunderground.com/api/25f1812b577e6bd0/conditions/q/"; // + city + ".json";
			url += state[i];
			url += "/";
			url += city;
			url += ".json";
			observer = current.getForObject(url, Weather.class);
			if ((observer.getCurrentObservation() != null)) {
				
				return observer;
			}
		}
		return new Weather(new CurrentObservation("Wrong City")) ;
	}

	@GetMapping(value = "/lol2")
	public CurrentObservation print2() {
		CurrentObservation result = new CurrentObservation();
		result.setWeather("cycki");
		return result;
	}

}
